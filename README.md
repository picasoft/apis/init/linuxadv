# Api/casoft Init Jour 2 : Linux avancé

## Description

Dépôt du support de la journée 2. Les sujets traités sont les suivants :
 - Linux, comment ça marche : démarrage, noyau, initrd, processus, init (systemd), montage des partitions, jusqu'à l'arrivée dans l'interface graphique et lancement d'un terminal
 - Swap : à quoi ça sert
 - Systèmes de fichier : réels et virtuels, fstab, proc, sys, dev, ou est rangé quoi (/bin, /sbin, /usr, /etc, /home etc), montage 'à la main' (loopback), automatique (hotplug + fuse), tout est fichier + permissions
 - Administration et diagnostics de base : devenir root, services, hostname, surveillance des processus (ps, top, htop, meminfo, free etc), consultation des logs (journalctl, dmesg)
 - Administration et diagnostics réseau : ping, host, ip
 - Diagnostics partitions : fsck, du, df
 - Ligne de commande : shells, tty, stdin, stdout, stderr, redirections, combinaisons de commande par pipe, variables d'environnement (en particulier PS, PATH) + héritage (export)
 - Identifier des fichiers, consulter des fichiers : file, cat, less (more)
 - Trouver des fichiers : find + actions
 - Rechercher dans les fichiers : grep
 - Manipuler le contenu de fichiers : sort, cut, tr, sed (basique...)
 - Archivage de dossiers (tar + compression)

**En cas de questions :** [stephane.bonnet@hds.utc.fr](mailto://stephane.bonnet@hds.utc.fr).

Pour aller plus loin : lire la [documentation de git](https://git-scm.com/docs).

En cas de remarques sur la présentation (ou de questions aussi), vous pouvez utiliser le [système d'issues](https://gitlab.utc.fr/picasoft/apis/h19/init/linuxadv/issues).

## Présentation

Une chaîne d'intégration permet de construire automatiquement le PDF à partir du fichier .tex présent à la racine.

Après construction (uniquement sur la branche `master`), le document final est disponible [à cette adresse](https://uploads.picasoft.net/api/linux_adv.pdf).

Le chemin d'upload est défini dans le fichier [.gitlab-ci.yml](./gitlab-ci.yml).

## Contribuer !

Créez une branche, et faites une merge request ! Faites la relire par un tiers qui la valide et la merge.
N'oubliez pas d'ajouter votre nom dans la licence et dans les auteurs de `main.tex`.
