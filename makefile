all:
	pdflatex main.tex
	pdflatex main.tex

presentation:
	pdflatex "\def\Release{PRESENTATION}\input{main}"
	pdflatex "\def\Release{PRESENTATION}\input{main}"

clean:
	rm -f *.nav *.aux *.dvi *.log *.out *.pdf *.snm *.toc *.vrb
