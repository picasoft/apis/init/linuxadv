\subsection{Plus sur les descripteurs...}

\begin{frame}[fragile,label=more_descriptors]{Les fichiers stdin, stdout, stderr existent-ils?}
	Sous Unix, tout est un fichier : données, programmes, périphériques comme le 
	clavier et l'écran sont des fichiers.
	
	\begin{block}{\cmd{tty}}
		La commande \cmd{tty} permet de voir à quel fichier sont associés les entrées et sorties standard.
		
		Il s'agit du terminal de contrôle du shell (\emph{controlling tty})
	\end{block}

	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
	\begin{Verbatim}
$ tty
/dev/pts/0
	\end{Verbatim}  
	\end{beamercolorbox}		
\end{frame}

\begin{frame}{Les fichiers de périphérique}
	
	Les fichiers de périphériques (\emph{device files}) comme \cmd{/dev/pts/0} sont des interfaces avec le noyau
	Linux.	
	
	Y accéder déclenche l'exécution d'un service du noyau, comme lire des caractères depuis le clavier ou afficher
	des caractères à l'écran, au travers d'un sous-système du noyau ou d'un pilote de périphérique.
	
	\begin{block}{\cmd{/dev}}
		Le répertoire \cmd{/dev} contient tous les fichiers de périphériques. 
		Ils peuvent être créés à la volée, par exemple après connexion d'une clé USB.
	\end{block}
\end{frame}

\begin{frame}[fragile]{Jouons avec /dev/pts}

	\begin{exampleblock}{}
		Ouvrez deux terminaux. Dans chaque, lancez \cmd{tty} pour observer leur terminal de contrôle.
	\end{exampleblock}
	
	\vspace{10pt}
	\begin{columns}[b]
		\begin{column}{.45\textwidth}
			Écrivez dans le terminal en redirigeant vers le terminal de contrôle de \textbf{l'autre} terminal
			\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
			\begin{Verbatim}
$ tty
/dev/pts/0
$ echo Salut > \textcolor{yellow}{/dev/pts/1}
			\end{Verbatim}  
			\end{beamercolorbox}
			\vspace{0pt}		
		\end{column}
		\begin{column}{.45\textwidth}
				Le texte tapé dans l'autre terminal s'affiche ici!
			\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
			\begin{Verbatim}
$ tty
\textcolor{yellow}{/dev/pts/1}
$ Salut
			\end{Verbatim}  
			\end{beamercolorbox}	
			\vspace{0pt}	
		\end{column}
    \end{columns}
    Retour aux \hyperlink{redirections}{\beamerbutton{redirections}}
\end{frame}

\subsection{Plus sur les expansions}

\begin{frame}[fragile,label=brace_expand]{Expansion d'accolades}
	\begin{block}{Expansion d'accolades}
		\cmd{bash} supporte l'expansion d'accolades: générer toutes les chaînes dans un intervalle ou à partir 
		d'une liste.
	\end{block}
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
	\begin{Verbatim}
$ echo \{1..20\}
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
$ echo a\{c,d,e\}f
acf adf aef
	\end{Verbatim}  
	\end{beamercolorbox}	

	\vspace{10pt}	
	Cette expansion peut être utilisée dans des motifs.
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
	\begin{Verbatim}
$ touch file_\{1..20\}
$ ls
file_1   file_12  file_15  file_18  file_20  file_5  file_8
file_10  file_13  file_16  file_19  file_3   file_6  file_9
file_11  file_14  file_17  file_2   file_4   file_7
$ rm file_\{12..14\}
$ ls
file_1   file_15  file_18  file_20  file_5  file_8
file_10  file_16  file_19  file_3   file_6  file_9
file_11  file_17  file_2   file_4   file_7
	\end{Verbatim}  
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]{Expansion d'accolades}
	\begin{block}{Extensions multiples}
		L'expansion d'accolade est très utilisée pour sélectionner des fichiers avec des extensions multiples.
		
		\cmd ls *.\{jpg,png,bmp\}
		
		listera tous les fichiers correspondants à ces extensions.
	\end{block}
	
	\begin{alertblock}{Extensions}
		\begin{itemize}
			\item Les extensions n'ont pas de sens particulier sous UNIX. 
			\item Spécificité de Windows et de ses prédécesseurs.
			\item Pratique pour indiquer le type des fichiers...
		\end{itemize}
    \end{alertblock}
    
    Retour aux \hyperlink{expansions}{\beamerbutton{expansions}}
\end{frame}

\subsection{Expressions régulières}

\begin{frame}[fragile, label=regex]{Expressions régulières (\emph{regex})}
	\begin{block}{}
		\begin{itemize}
			\item Chaîne de caractères qui décrit précisément un ensemble 
			de chaînes de caractères possibles.
			\item Dans les commandes qui utilisent un motif (comme \cmd{grep}), le motif est presque toujours une expression régulière.
		\end{itemize}
	\end{block}
\end{frame}
	
\begin{frame}[fragile]{Expressions régulières (\emph{regex})}	
	\framesubtitle{Bases}
		\begin{itemize}
			\item Décrit la suite de caractères qui doivent correspondre dans une chaîne.
			\item Les alternatives sont séparées par une barre verticale: \cmd{a|b} reconnaît tout
			mot contenant 'a' ou 'b'.
			\item Les parenthèses regroupent les expressions:
			\begin{itemize}
				\item \cmd{ab|cd} reconnaît tout mot contenant 'ab' ou 'cd'
				\item \cmd{a(b|c)d} reconnaît tout mot contenant 'abd' ou 'acd'.
			\end{itemize}
		\end{itemize}
		
		\begin{alertblock}{Attention aux caractères interprétés par le shell}
			Les parenthèses et les barres verticales sont interprétées par le shell.
			Il faut les échapper pour les utiliser dans des regex. Pour exploiter le motif 
			\cmd{a(b|c)d} avec grep, il faudra écrire:
			\begin{center}
				\cmd{grep 'a\textbackslash(b\textbackslash|c\textbackslash)d'}
			\end{center}
		\end{alertblock}
\end{frame}

\begin{frame}[fragile]{Expressions régulières (\emph{regex})}	
	\framesubtitle{Caractères}
		Chaque caractère de la chaîne à reconnaître doit être spécifié, soit en donnant sa valeur 
		directement ou en donnant sa classe.
		\begin{itemize}
			\item	\cmd{abcd} : la	suite 'abcd'.
			\item	\cmd{[ab]bcd} : soit a, soit b suivi de bcd. Les crochets définissent une 
			classe de caractères acceptables pour un caractère donné.
			\item	\cmd{[a-z]bcd} : on peut utiliser des intervalles dans les classes.
			\item	\cmd{a.b} : trois caractères commençant par a et finissant par b. 
			Le point correspond à n'importe quel caractère.
			\item	\cmd{[:alnum:]} : tout caractère alphanumérique
			\item	\cmd{[:digit:]} : tout chiffre
			\item	\cmd{[:space:]} : tout espace
		\end{itemize}
\end{frame}

\begin{frame}[fragile]{Expressions régulières (\emph{regex})}	
	\framesubtitle{Quantificateurs et prédicats}
		Il représentent le nombre de répétitions d'une expression et ajoutent des contraintes sur
		sa position dans la chaîne.

		\begin{itemize}
			\item	\cmd{expr*} : le motif \cmd{expr} répété 0 ou plusieurs fois.
			\item   \cmd{expr+} : le motif \cmd{expr} répété 1 ou plusieurs fois.
			\item	\cmd{expr?} : le motif \cmd{expr} répété 0 ou une fois.
			\item	\verb+^expr+ : le motif \cmd{expr} en début de ligne.
			\item	\cmd{expr\$} : le motif \cmd{expr} en fin de ligne.
		\end{itemize}
\end{frame}

\begin{frame}[fragile]{\cmd{grep} et expressions régulières}
	\begin{exampleblock}{Trouver tous les mots commençant par ab}
		\verb+grep '^ab' /usr/share/dict/words+
	\end{exampleblock}
	
	\begin{exampleblock}{Trouver tous les mots de quatre lettres commençant par g et finissant par t}
		\verb+grep '^g..t$' /usr/share/dict/words+
	\end{exampleblock}
	
	\begin{exampleblock}{Trouver tous les mots commençant par g et finissant par t}
		\verb,grep '^g.\+t$' /usr/share/dict/words,
	\end{exampleblock}
	
	\begin{alertblock}{C'est très pratique...}
		...pour les mots croisés!
    \end{alertblock}
    
    Retour à \hyperlink{grep}{\beamerbutton{grep}}
\end{frame}
