\subsection{Utilisateurs et groupes}

\begin{frame}[fragile]{Utilisateurs et groupes}
	\framesubtitle{Vocabulaire}
	Unix est un système multi-utilisateurs. Les utilisateurs peuvent appartenir à des groupes.

	\begin{block}{Utilisateur}
		Deux catégories:
		\begin{itemize}
			\item Utilisateurs \emph{système} utilisés pour la sécurité du système.
			\item Utilisateurs \emph{normaux} qui sont voués à se servir du système.
		\end{itemize}
		Un utilisateur est identifié par un nombre, l'\textbf{uid} (\emph{user id}) auquel est 
		associé un nom, le \textbf{username} ou \textbf{login}.
	\end{block}
	
	\begin{block}{Groupe}
		Les utilisateurs appartiennent à un groupe principal et peuvent appartenir 
		à plusieurs groupes secondaire. Les groupes sont définis par
		un numéro, le \textbf{gid} (\emph{group id}) auquel est associé un nom.
	\end{block}
\end{frame}

\begin{frame}[fragile]{Utilisateurs et groupes}
	\framesubtitle{Fichiers associés}
	Les fichiers de gestion des utilisateurs sont stockés dans le répertoire \cmd{/etc}.

	\begin{itemize}
		\item Le fichier \cmd{/etc/passwd} contient la liste des utilisateurs.
		\item Le fichier \cmd{/etc/shadow} contient les mots de passe chiffrés des utilisateurs.
		\item Le fichier \cmd{/etc/groups} contient la liste des groupes et de leurs membres.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Utilisateurs et groupes}
	\framesubtitle{Utilisateurs et groupes propriétaires}
	
	Les fichiers et répertoires sont tous la propriété d'utilisateurs variables. L'immense majorité des fichiers
	appartient à l'utilisateur \emph{root}.
	
	La commande ``\cmd{ls -l}'' permet d'afficher le propriétaire d'un fichier.
	\begin{beamercolorbox}[wd=\linewidth,rounded=true,shadow=true]{terminal}
    \begin{Verbatim}
$ ls -l /etc/shadow
-rw-r----- 1 root shadow 1296 janv.  4 12:15 /etc/shadow
	\end{Verbatim}
    \end{beamercolorbox}
    
    Ici, le fichier \cmd{/etc/shadow} appartient à l'utilisateur \cmd{root} et au groupe \cmd{shadow}.
\end{frame}

\begin{frame}[fragile]{Utilisateurs et groupes}
	\framesubtitle{Changement de propriétaire}

	Seul le superutilisateur peut changer le propriétaire de fichiers ou de répertoires au moyen
	des commandes suivantes:	
	\begin{itemize}
		\item \cmd{chown} change l'utilisateur propriétaire (et éventuellement le groupe)
		\item \cmd{chgrp} change le groupe propriétaire
	\end{itemize}		
	
	\begin{exampleblock}{}
		\cmd{chown newuser:newgroup	/my/file}
		\cmd{chgrp newgroup	/my/file}
	\end{exampleblock}
\end{frame}


\begin{frame}[fragile]{Qui suis-je?}
	Les commandes suivantes permettent d'identifier l'utilisateur courant et les groupes auxquel il est associé.

	\begin{description}
		\item \cmd{id}
		\item \cmd{whoami}
		\item \cmd{groups}
	\end{description}
	
	\begin{beamercolorbox}[wd=\linewidth,rounded=true,shadow=true]{terminal}
    \begin{Verbatim}
$ id
uid=1000(bonnetst) gid=1000(bonnetst) groupes=1000(bonnetst),
4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),116(lpadmin),126(sambashare)
$ whoami
bonnetst
$ groups
bonnetst adm cdrom sudo dip plugdev lpadmin sambashare
	\end{Verbatim}
    \end{beamercolorbox}
\end{frame}


\begin{frame}{Devenir grand chef}
	\framesubtitle{Utilisateurs et processus}
	\begin{itemize}
		\item Un nouveau processus hérite de l'identité du processus qui l'a lancé.
		\item S'il est lancé par le shell d'un utilisateur, il hérite de l'identité de cet utilisateur.
		\item Le processus ne peut accéder qu'aux ressources qui sont accessibles à cet utilisateur.
	\end{itemize}

	\begin{block}{Superutilisateur: \cmd{root}}
		Un utilisateur a accès à toutes les ressources du système : l'utilisateur \cmd{root} d'uid 0.
	\end{block}
	
\end{frame}

\begin{frame}{Devenir grand chef}
	\framesubtitle{sudo et su}
	
	Sur Ubuntu, \cmd{root} ne peut pas se connecter directement, il faut un moyen pour qu'un utilisateur
	normal puisse accéder aux ressources et fichiers protégés.
	
	\begin{block}{\cmd{sudo}}
		\begin{itemize}
			\item Exécute une commande sous l'identité du superutilisateur.
			\item Ne peut être utilisée que par les membres du groupe \cmd{sudo}.
			\item ``\cmd{sudo bash}'' lance un shell superutilisateur.
		\end{itemize}
	\end{block}

	\begin{block}{\cmd{su}}
		\begin{itemize}
			\item Sur d'autres systèmes, la commande \cmd{su} (\emph{Substitute User}) est utilisée.
			\item Substitue l'utilisateur \cmd{root} à l'utilisateur courant.
			\item Nécessite d'entrer le mot de passe de l'utilisateur root.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Création et suppression}
	\begin{itemize}
		\item \cmd{adduser} Ajoute un utilisateurs
		\item \cmd{deluser} Supprime un utilisateur
		\item \cmd{addgroup} Ajoute un groupe
		\item \cmd{delgroup} Supprime un utilisateur
	\end{itemize}
	
	\begin{block}{Ajouter et supprimer des utilisateurs aux groupes}
		\begin{itemize}
			\item \cmd{adduser user group} ajoute l'utilisateur \cmd{user} au groupe \cmd{group} 
			\item \cmd{deluser user group} supprime l'utilisateur \cmd{user} du groupe \cmd{group}
		\end{itemize}
	\end{block}
	
\end{frame}

\begin{frame}{Changer son mot de passe}
	La commande ``\cmd{passwd}'' permet de changer son mot de passe.
	
	\begin{alertblock}{Changement pour autrui}
		Le superutilisateur peut changer le mot de passe d'autres utilisateurs.
		
		``\cmd{passwd user}'' change le mot de passe de l'utilisateur \cmd{user}.
	\end{alertblock}
\end{frame}

\subsection{Permissions}

\begin{frame}{Les permissions}
	Les fichiers et répertoires appartiennent à un utilisateur et à un groupe. Les permissions d'accès sont définies pour:
	
	\begin{itemize}
		\item Le propriétaire du fichier (\emph{user}): \textbf{(u)}.
		\item Le groupe du fichier: \textbf{(g)}.
		\item Le reste du monde (\emph{others}): \textbf{(o)}.
	\end{itemize}

	Les permissions d'accès peuvent être en:
	\begin{itemize}
		\item Lecture (\emph{read}): \textbf{(r)}.
		\item Écriture (\emph{write}): \textbf{(w)}.
		\item Exécution (\emph{eXecute}): \textbf{(x)}.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Visualisation des permissions}
	
	``\cmd{ls -l}'' affiche les permissions d'un fichier ou d'un répertoire.
	\begin{beamercolorbox}[wd=\linewidth,rounded=true,shadow=true]{terminal}
    \begin{Verbatim}
	$ cd ~/api
	$ ls -l
	-rw-r--r-- 1 bonnetst bonnetst    0 janv. 27 21:03 file_1
	drwxr-xr-x 2 bonnetst bonnetst 4096 janv. 27 21:03 \textbf{\textcolor{cyan}{repertoire}}
	\end{Verbatim}
	\end{beamercolorbox}	
	
	Un répertoire est repéré par le \verb#d# en début de permissions.
\end{frame}

\begin{frame}[fragile]
	\frametitle{Interprétation}
	
	\begin{itemize}
		\item -\textcolor{blue}{rw-}r-{}-r-{}- indique les permissions pour le propriétaire
		\item -rw-\textcolor{blue}{r-{}-}r-{}- indique les permissions pour le groupe
		\item -rw-r-{}-\textcolor{blue}{r-{}-} indique les permissions pour les autres
	\end{itemize}

	\begin{itemize}
		\item Dans le cas des \textbf{fichiers}, elles indiquent ce qu'on a le droit de faire \emph{dans le fichier}
		\item Dans le cas des \textbf{répertoires}, elles indiquent ce qu'on a le droit de faire \emph{dans le répertoire}
		\begin{itemize}
			\item \verb#r# permet de lister le contenu du répertoire
			\item \verb#w# permet de créer ou de supprimer des fichiers (indépendamment des permissions du fichier!)
			\item \verb#x# permet de se placer dans le répertoire ou de le traverser sur un chemin.
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{\cmd{chmod}}
	\begin{block}{}
		\begin{description}
			\item[\cmd{chmod}] affecte (\verb#=#), ajoute (\verb#+#)  ou supprime (\verb#-#) des permissions
		\end{description}
	\end{block}

	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
	\begin{Verbatim}
$ cd ~/api
$ ls -l
-rw-r--r-- 1 bonnetst bonnetst    0 janv. 27 21:03 file_1
$ \textbf{\textcolor{yellow}{chmod g+w}} file_1
$ ls -l
-rw-rw-r-- 1 bonnetst bonnetst    0 janv. 27 21:03 file_1
$ \textbf{\textcolor{yellow}{chmod g=r}} file_1
$ ls -l
-rw-r--r-- 1 bonnetst bonnetst    0 janv. 27 21:03 file_1
$ \textbf{\textcolor{yellow}{chmod g-r}} file_1
$ ls -l
-rw----r-- 1 bonnetst bonnetst    0 janv. 27 21:03 file_1
	\end{Verbatim}
	\end{beamercolorbox}

On peut les combiner:
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
	\begin{Verbatim}
$ cd ~/api
$ \textbf{\textcolor{yellow}{chmod u+x,g+wx,o-r}} file_1
$ ls -l
-rwxrwx--- 1 bonnetst bonnetst    0 janv. 27 21:03 \textbf{\textcolor{green}{file_1}}
	\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\subsection{Variables}
\begin{frame}[fragile]{Les variables}
	Le shell permet de définir et d'utiliser des variables.
	
	\begin{block}{Variable}
		\begin{itemize}
			\item Identifiée par son nom
			\item Affectée d'une valeur
			\item Elle peut être locale au shell
			\item Elle peut être exportée. Dans ce cas, on parle de variable d'environnement.
		\end{itemize}
	\end{block}
	
	\begin{block}{Manipulation des variables}
		\begin{itemize}
			\item \cmd{myvar="hello"} Crée la variable \cmd{myvar} et y affecte la chaîne "hello".
			\item \cmd{export MYVAR="hello"} Crée la variable d'environnement \cmd{MYVAR}.
			\item \cmd{unset myvar} Supprime la variable \cmd{myvar}.
			\item \cmd{\$myvar} accède à la valeur de la variable \cmd{myvar}.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}[fragile]{Les variables}
		\framesubtitle{Exemple}
		\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
		\begin{Verbatim}
$ myvar="hello"
$ echo $myvar
hello
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]{Export de variables}

	\begin{block}{Export}
		Une variable exportée est connue de tous les processus lancés à partir du shell où elle a été définie.
		Elle est héritée par tous les processus enfants. Elle fait partie de leur environnement.
	\end{block}
	
	\begin{beamercolorbox}[wd=\linewidth,rounded=true,shadow=true]{terminal}
    \begin{Verbatim}
$ myvar="hello"
$ export MYVAR="hello export"
$ bash   \textcolor{yellow}{<----- A partir de ce point, on est dans un second shell}
$ echo $myvar

$ echo $MYVAR
hello export
	\end{Verbatim}
    \end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]{Variable PATH}

	\begin{itemize}
		\item Spécifie les chemins où seront recherchées les commandes
		\item Variable extrêmement importante. Si un répertoire qui contient des commandes n'est pas dans le PATH, 
		il faudra spécifier son chemin complet pour pouvoir la lancer.
	\end{itemize}
	
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
    \begin{Verbatim}
$ echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
$ which ls
/bin/ls
	\end{Verbatim}
    \end{beamercolorbox}
    	
	\begin{block}{\cmd{which}}
		\cmd{which} affiche l'emplacement d'une commande. 
		Recherche dans tous les répertoires du PATH.
	\end{block}

\end{frame}

\begin{frame}[fragile]{Variable PS1}

	\begin{itemize}
		\item Spécifie le format du prompt du shell.
		\item On peut tenter de la redéfinir pour personnaliser sont shell.
		\item RTFM!
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Valeurs de retour}

	\begin{block}{Valeur de retour}
		\begin{itemize}
			\item Toutes les commandes retournent une valeur qui indique qu'elles ont réussi ou échoué.
			\item La valeur 0 indique le succès, toutes les autres valeurs l'échec.
			\item La variable du shell \cmd{\$?} contient cette valeur à la fin de chaque commande.
		\end{itemize}
	\end{block}
	
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
    \begin{Verbatim}
$ true
$ echo $?
0
$ false
$ echo $?
1
	\end{Verbatim}
    \end{beamercolorbox}
\end{frame}

\subsection{Fichiers de configuration}
\begin{frame}[fragile]{Les différents fichiers de configuration de \cmd{bash}}
	\cmd{bash} exploite plusieurs fichiers de configuration:
	
	\begin{itemize}
		\item \cmd{/etc/bash.bashrc} est le fichier de configuration global, commun à tous les utilisateurs
		\item \verb+~/.bashrc+ est le fichier de configuration spécifique à l'utilisateur
	\end{itemize}
	
	\begin{block}{\cmd{.bashrc}}
		Les commandes de ce fichier sont exécutées par \cmd{bash} au démarrage.
		
		Il suffit donc d'y ajouter toute personnalisation pour qu'elle soit prise en compte.
	\end{block}
\end{frame}